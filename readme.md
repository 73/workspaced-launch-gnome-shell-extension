# 🚀 WORKSPACED LAUNCH

This is a GNOME Shell extension that launches a new window when clicking in the dash or the application view if there is none on the current workspace.

## INSTALLATION
Install from: https://extensions.gnome.org/extension/3745/workspaced-launch/

## BASED ON
  - https://github.com/N-Yuki/gnome-shell-extension-workspace-isolated-dash
  - https://extensions.gnome.org/extension/600/launch-new-instance/
