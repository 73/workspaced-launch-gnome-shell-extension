// based on https://github.com/N-Yuki/gnome-shell-extension-workspace-isolated-dash 

const Shell = imports.gi.Shell;

function enable() {
	Shell.App.prototype._workspace_isolated_dash_nyuki_activate = Shell.App.prototype.activate;
	Shell.App.prototype.activate = function() {
		if (this.is_on_workspace(global.workspaceManager.get_active_workspace())) {
			return this._workspace_isolated_dash_nyuki_activate();
		}
		return this.open_new_window(-1);
	};
}

function disable() {
	if (Shell.App.prototype._workspace_isolated_dash_nyuki_activate) {
		Shell.App.prototype.activate = Shell.App.prototype._workspace_isolated_dash_nyuki_activate;
		delete Shell.App.prototype._workspace_isolated_dash_nyuki_activate;
	}
}

function init(meta) {
	/* do nothing */
}
